<?php

namespace Technify\Services\Observer\Order;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UpdateOrder implements ObserverInterface{


    /** @var \Technify\Services\Helper\Help $helper*/
    public $helper;

    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager*/
    private $_objectManager;

    /** @var \Magento\Sales\Model\OrderRepository $_orderRepository*/
    private $_orderRepository;


    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Technify\Services\Helper\Help $helper
    )
    {
        $this->_orderRepository = $orderRepository;
        $this->_objectManager = $objectManager;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getData('order');
        $incrementId = $this->helper->getParentOrderId($order->getIncrementId());
        $this->pushOrder('order',$incrementId,'update',$this->helper->getTechnifyStoreId(),"pushOrderObject");
    }

    /**
     * @param string $entity
     * @param string|int $entityId
     * @param string $event
     * @param string $storeId
     * @param string $action
     */
    public function pushOrder($entity, $entityId, $event, $storeId, $action)
    {
        $data['entity'] = array(
            'name'  	=> $entity,
            'id'    	=> $entityId,
            'event' 	=> $event,
            'store_id'	=> $storeId
        );
        $this->_objectManager->get('\Technify\Services\Helper\RequestGateway')->push($action,$data);

    }
}