<?php

namespace Technify\Services\Helper;

use \Magento\Sales\Model\Order as SalesOrderModel;

class Order{


    /** @var \Magento\Sales\Model\OrderRepository $_orderRepository */
    private $_orderRepository;

    /** @var \Magento\Sales\Model\Order $order */
    private $order;

    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager */
    private $_objectManager;


    /**
     * Order constructor.
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_orderRepository = $orderRepository;
    }


    /**
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOrder()
    {
        return $this->_orderRepository->get($this->order->getId());
    }

    /**
     * @param \Technify\Services\Api\OrderStatusInterface $orderStatus
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateOrderStatus(\Technify\Services\Api\OrderStatusInterface $orderStatus)
    {
        switch ($orderStatus->getState())
        {
            case SalesOrderModel::STATE_COMPLETE:
            case SalesOrderModel::STATE_CANCELED:
            case SalesOrderModel::STATE_CLOSED:
            case SalesOrderModel::STATE_HOLDED:
            case SalesOrderModel::STATE_PROCESSING:
                $this->changeOrderState($orderStatus);
                $this->order->save();
            break;
            case SalesOrderModel::ACTION_FLAG_SHIP:
                $this->updateOrderStateToShip($orderStatus);
                break;
            default:break;
        }

        return $this;
    }

    /**
     * @param \Technify\Services\Api\OrderStatusInterface $orderStatus
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateOrderStateToShip($orderStatus){

        if (! $this->order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You cant create the Shipment.') );
        }

        $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
        $shipment = $convertOrder->toShipment($this->order);

        foreach ($this->order->getAllItems() AS $orderItem)
        {
            if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $orderItem->setIsVirtual(0);
            $qtyShipped = $orderItem->getQtyToShip();

            $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

            $shipmentItem
                ->setOrderItemId($orderItem->getId())
                ->setRowTotal($orderItem->getRowTotal());

            $shipment->addItem($shipmentItem);
        }

        $shipment->setTracks($orderStatus->getTracks());
        $shipment->addComment($orderStatus->getComment());
        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);
        try {
            $shipment->save();
            $shipment->getOrder()->save();
            $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                ->notify($shipment);
            $shipment->save();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }


    /**
     * @param \Technify\Services\Api\OrderStatusInterface $orderStatus
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function changeOrderState(\Technify\Services\Api\OrderStatusInterface $orderStatus)
    {
        $orderService = $this->_objectManager->get('\Magento\Sales\Model\Service\OrderService');

        $orderService
            ->setState(
                $this->order,
                $orderStatus->getState(),
                $orderStatus->getStatus(),
                $orderStatus->getComment(),
                $orderStatus->getNotify(),
                false
            );
    }
}