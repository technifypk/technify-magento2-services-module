<?php
namespace Technify\Services\Api;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
interface OrderInterface
{

    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */

    const ORDER_ID = 'order_id';

    const ORDER_STATUS = 'order_status';

    /**
     * @param string $orderId
     * @param \Technify\Services\Api\OrderStatusInterface $orderStatus
     * @return \Magento\Sales\Api\Data\OrderInterface
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */

    public function update(
        $orderId,
        \Technify\Services\Api\OrderStatusInterface $orderStatus
    );


    /**
     * @param string $orderId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function get($orderId);
}