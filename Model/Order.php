<?php

namespace Technify\Services\Model;

use Technify\Services\Api\OrderInterface;
use Technify\Services\Api\OrderStatusInterface;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
class Order extends \Technify\Services\Helper\Order implements OrderInterface
{
    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager*/
    private $_objectManager;


    /** @var \Magento\Sales\Model\OrderRepository $_orderRepository */
    private $_orderRepository;

    /** @var \Technify\Services\Helper\Help $_helper*/
    private $_helper;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Technify\Services\Helper\Help $helper
    )
    {
        $this->_helper = $helper;
        $this->_orderRepository = $orderRepository;
        $this->_objectManager = $objectManager;

        parent::__construct($orderRepository,$objectManager);
    }

    /**
     * @param string $orderId
     * @param OrderStatusInterface $orderStatus
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function update(
        $orderId,
        OrderStatusInterface $orderStatus
    )
    {
        $order = $this->_objectManager
            ->create(
                'Magento\Sales\Model\Order'
            )->loadByIncrementId($orderId);


        $this->setOrder($order)->updateOrderStatus($orderStatus);

        return $this->getOrder();
    }

    /**
     * @param string $orderId
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($orderId)
    {
        $incrementId = $this->_helper->getOrderLastChild($orderId);

        $order = $this->_objectManager
            ->create(
                'Magento\Sales\Model\Order'
            )->loadByIncrementId($incrementId);

        $orderResponse = $this->_orderRepository->get(
            $order->getId()
        );

        $orderResponse->setIncrementId($orderId);

        return $orderResponse;
    }
}